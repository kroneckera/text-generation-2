from .text_generator import TextGeneratorModel, TextGenerator
import argparse


def prettify(text):
    for num, token in enumerate(text):
        if num > 0 and token not in '.?!,':
            text[num] = ' ' + text[num]
    return ''.join(text)


def create_model(args):
    text_generator = TextGenerator()
    data = text_generator.prepare_data(
        args.data_path, extension=args.extension
    )
    text_generator.build_model(
        data, ngram_size=args.ngram_size,
        smoothing=args.smoothing, smoothing_alpha=args.smoothing_alpha,
        save_path=args.save_path
    )


def generate_text(args):
    text_generator = TextGenerator()
    text = text_generator.generate(
        args.length, saved_model_path=args.model_path
    )
    print(prettify(text))


def parse_args():
    parser = argparse.ArgumentParser(
        description='Command line tool for text_generator module.'
    )
    subparsers = parser.add_subparsers()

    parser_create_model = subparsers.add_parser(
        'create_model', help='Create model from the given data'
    )
    parser_create_model.add_argument(
        '--data_path', help='The path of the data. It could be a directory '
        'or a single file. In case of directory, data is accumulated from all'
        ' files of the directory tree with given extension. Otherwise, single'
        ' file is used regardless of file extension.'
    )
    parser_create_model.add_argument(
        '--save_path',
        help='The path to save the created model.',
        default='model.pkl'
    )
    parser_create_model.add_argument(
        '--extension', help='If data_path is a directory then only files with '
        'this extention is used for data generation.',
        default='.txt'
    )
    parser_create_model.add_argument(
        '--ngram_size', help='The size of N-gram model.',
        type=int,
    )
    parser_create_model.add_argument(
        '--smoothing', help='If the model uses smoothing. If not, the model '
        'skips edges from markov chain if absorbing state is not reachable'
        ' from them. Otherwise, stupid backoff is used.',
        action='store_true'
    )
    parser_create_model.add_argument(
        '--smoothing_alpha', help='The parameter in the stupid backoff.',
        type=float, default=0.4,
    )
    parser_create_model.set_defaults(func=create_model)

    parser_generate_text = subparsers.add_parser(
        'generate_text',
        help='Generates text of given length from saved model.'
    )
    parser_generate_text.add_argument(
        '--model_path', help='The path to the model. Default is model.pkl.',
        default='model.pkl'
    )
    parser_generate_text.add_argument(
        '--length',
        help='The number of token in generated text. Default is 250.',
        type=int, default=250
    )
    parser_generate_text.set_defaults(func=generate_text)
    parser_create_model.set_defaults(func=create_model)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    args.func(args)
